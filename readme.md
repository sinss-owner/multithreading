# MultiThreading

．Main Thread (UI Thread) , background thread
．PerformSelector , NSThread
．GCD
．NSOperation + NSOperationQueue
．NSRunloop
    接受消息->等待->處理循環
    func loop() {
        init()
        do {
            process(message)
        } while (hasMessage)
    }

．觀察者模式 (Observer Pattern), KVO,  NSNotificationCenter



# Block & Delegate

．Delegate
    - Define a Delegate
    - Usage of Delegate
．Block
    - Define a block
    - Useage of block
．Delegate VS Block
