//
//  Job.m
//  MultiThreading
//
//  Created by leo.chang on 19/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import "Job.h"

@implementation Job

- (void)dealloc
{
    NSLog(@"Job released");
}

- (void)job1
{
    sleep(3);
    if ([self.dataSource respondsToSelector:@selector(whoExecuteMe)])
    {
        NSString *executor = [self.dataSource whoExecuteMe];
        NSLog(@"Executor: %@", executor);
    }
    NSLog(@"Job1 Finish! isMainThread: %@", [NSThread isMainThread] ? @"YES": @"NO");
}

- (void)job2
{
    sleep(2);
    NSLog(@"Job2 Finish! isMainThread: %@", [NSThread isMainThread] ? @"YES": @"NO");
}

- (void)job3:(NSString*)param
{
    sleep(1);
    if ([self.dataSource respondsToSelector:@selector(whoExecuteMe)])
    {
        NSString *executor = [self.dataSource whoExecuteMe];
        NSLog(@"Executor: %@", executor);
    }
    
    NSLog(@"Job3 Finish! isMainThread:%@ : param: %@", [NSThread isMainThread] ? @"YES": @"NO", param);
}

- (void)job4:(NSString *)param completion:(completionHandler)completion
{
    sleep(4);
    if ([self.dataSource respondsToSelector:@selector(whoExecuteMe)])
    {
        NSString *executor = [self.dataSource whoExecuteMe];
        NSLog(@"Executor: %@", executor);
    }
    NSError *error = [NSError errorWithDomain:@"tw.com.chinalife.app" code:500 userInfo:@{NSLocalizedFailureReasonErrorKey: param}];
    completion(nil, error);
    if ([self.delegate respondsToSelector:@selector(job4CompleteSuccess:failure:)])
    {
        [self.delegate job4CompleteSuccess:nil failure:error];
    }
}

- (void)job5:(NSNumber *)param success:(SuccessHandler)success failure:(FailureHandler)failure
{
    sleep(5);
    if ([self.dataSource respondsToSelector:@selector(whoExecuteMe)])
    {
        NSString *executor = [self.dataSource whoExecuteMe];
        NSLog(@"Executor: %@", executor);
    }
    NSFileManager *fm = NSFileManager.defaultManager;
    NSError *error = nil;
    NSString *url = [[NSBundle mainBundle] pathForResource:@"main" ofType:@"m"];
    [fm removeItemAtPath:url error:&error];
    if (error)
    {
        failure(error);
    }
    else
    {
        NSString *str = [NSString stringWithFormat:@"%@ 刪除檔案成功", param];
        success(str);
    }
}

+ (void)job6:(NSNumber *)param completion:(void (^)(NSString *))completion
{
    sleep(6);
    NSString *str = [NSString stringWithFormat:@"%@ job6完成", param];
    completion(str);
}

- (void)job7:(NSString *)param completion:(ReturnHandler)completion
{
    sleep(5);
    if ([self.dataSource respondsToSelector:@selector(whoExecuteMe)])
    {
        NSString *executor = [self.dataSource whoExecuteMe];
        NSLog(@"Executor: %@", executor);
    }
    NSString *returnStr = completion(param);
    NSLog(@"returned block: %@", returnStr);
}

@end
