//
//  CustomOperation.m
//  MultiThreading
//
//  Created by leo.chang on 19/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import "CustomOperation.h"
#import "Job.h"

@implementation CustomOperation

- (instancetype)initWithParam:(NSString*)param
{
    self = [super init];
    if (self)
    {
        _param = param;
    }
    return self;
}

- (void)start
{
    NSLog(@"start");
    Job *job = [Job new];
    [job job3:self.param];
}

- (void)main
{
    NSLog(@"main");
    Job *job = [Job new];
    [job job3:self.param];
}

- (void)updateData
{
    NSLog(@"CustomOperation notified");;
}

@end
