//
//  Job.h
//  MultiThreading
//
//  Created by leo.chang on 19/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 _NSConcretGlobalBlock    Global
 _NSConcretStackBlock     函式內部
 _NSConcretMallocBlock    使用了Property
 */

typedef void (^completionHandler) (NSArray *items, NSError *error);
typedef void (^SuccessHandler) (NSString  *items);
typedef void (^FailureHandler) (NSError  *error);
typedef NSString* (^ReturnHandler) (NSString *error);

@protocol JobDelegate, JobDataSource;
@interface Job : NSObject

@property (nonatomic, assign) NSInteger callbyvalue;
@property (nonatomic, strong) NSNumber  * callbyreference;

@property (nonatomic, weak) id<JobDelegate> delegate;
@property (nonatomic, weak) id<JobDataSource> dataSource;

- (void)job1;
- (void)job2;
- (void)job3:(NSString*)param;

/**
 @param param 參數
 @param completion callback
 */
- (void)job4:(NSString*)param completion:(completionHandler)completion;

/**
 @param param 參數
 @param success 成功
 @param failure 失敗
 */
- (void)job5:(NSNumber*)param success:(SuccessHandler)success failure:(FailureHandler)failure;

/**
 
 */
+ (void)job6:(NSNumber*)param completion:(void (^)(NSString *error))completion;

- (void)job7:(NSString*)param completion:(ReturnHandler)completion;

@end

@protocol JobDelegate <NSObject>

- (void)job4CompleteSuccess:(NSString*)success failure:(NSError*)error;

@end

@protocol JobDataSource <NSObject>

- (NSString*)whoExecuteMe;

@end
