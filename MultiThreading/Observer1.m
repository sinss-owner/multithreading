//
//  Observer1.m
//  MultiThreading
//
//  Created by leo.chang on 19/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import "Observer1.h"

@implementation Observer1

- (void)updateData
{
    NSLog(@"Observer1 notified");
}

@end
