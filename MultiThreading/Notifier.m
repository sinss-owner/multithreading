//
//  Notifier.m
//  MultiThreading
//
//  Created by leo.chang on 19/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import "Notifier.h"

@interface Notifier()

@property (nonatomic, strong) NSMutableArray *observers;

@end

@implementation Notifier

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _observers = [NSMutableArray array];
    }
    return self;
}

- (void)notify
{
    for (id obj in _observers)
    {
        if ([obj respondsToSelector:@selector(updateData)]) {
            [obj updateData];
        }
    }
}

- (void)addObserver:(id<ObserverProtocol>)observer
{
    [_observers addObject:observer];
}

- (void)removeLast
{
    [_observers removeLastObject];
}

@end
