//
//  ObserverProtocol.h
//  MultiThreading
//
//  Created by leo.chang on 19/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ObserverProtocol <NSObject>

- (void)updateData;

@end
