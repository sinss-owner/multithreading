//
//  CustomOperation.h
//  MultiThreading
//
//  Created by leo.chang on 19/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObserverProtocol.h"

@interface CustomOperation : NSOperation <ObserverProtocol>

@property (nonatomic, strong) NSString *param;

- (instancetype)initWithParam:(NSString*)param;

@end
