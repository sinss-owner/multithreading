//
//  CustomControl.m
//  MultiThreading
//
//  Created by leo.chang on 24/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import "CustomControl.h"

@implementation CustomControl

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
    }
    return self;
}

- (void)initUI_block
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    button = self.onConfigured(button);
}

- (void)initUI_delegate
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    if ([self.delegate respondsToSelector:@selector(configureButton:)])
    {
        button = [self.delegate configureButton:button];
    }
}

@end
