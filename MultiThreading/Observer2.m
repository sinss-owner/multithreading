//
//  Observer2.m
//  MultiThreading
//
//  Created by leo.chang on 19/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import "Observer2.h"

@implementation Observer2

- (void)updateData
{
    NSLog(@"Observer2 notified");
}

@end
