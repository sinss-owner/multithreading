//
//  CustomControl.h
//  MultiThreading
//
//  Created by leo.chang on 24/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef UIButton* (^configureBlock) (UIButton *control);

@protocol CustomControlDelegate;
@interface CustomControl : UIControl

@property (nonatomic, copy) configureBlock onConfigured;
@property (nonatomic, weak) id<CustomControlDelegate> delegate;

@property (nonatomic, strong) UIButton *button;

- (void)initUI_block;
- (void)initUI_delegate;

@end

@protocol CustomControlDelegate <NSObject>

- (UIButton*)configureButton:(UIButton*)button;

@end
