//
//  ViewController.m
//  MultiThreading
//
//  Created by leo.chang on 19/10/2017.
//  Copyright © 2017 ChinaLife. All rights reserved.
//

#import "ViewController.h"
#import "Notifier.h"
#import "Job.h"
#import "CustomOperation.h"
#import "Observer1.h"
#import "Observer2.h"
#import "CustomControl.h"
#include <stdlib.h>

@import WebKit;

static void * kvoContext = &kvoContext;

@interface ViewController () <JobDelegate, JobDataSource, CustomControlDelegate>

@property (nonatomic, strong) Notifier *notifier;
@property (nonatomic, strong) Job *job;
@property (nonatomic, strong) WKWebView *webview;

@property (nonatomic, strong) NSString *str;


@end

@implementation ViewController

- (void)dealloc
{
    NSLog(@"VC dealloc");
}

- (void)dosomething
{
    NSLog(@"!!!");
}

//Accessors (Lazy loader)
- (Notifier*)notifier
{
    if (!_notifier)
    {
        _notifier = [Notifier new];
    }
    return _notifier;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //KVO observer
    _job = [Job new];
    _str = @"123";
    
    CustomControl *control = [CustomControl new];
    control.onConfigured = ^UIButton *(UIButton *button) {
        [button setTitle:@"block configured" forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        return button;
    };
    
    control.delegate = self;
}

//MARK: - CustomControlDelegate
- (UIButton*)configureButton:(UIButton *)button
{
    [button setTitle:@"block configured" forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    return button;
}

- (void)addObserver
{
    /*
     所有繼承NSObject皆支援 
     */
    [_job addObserver:self forKeyPath:@"callbyvalue" options:NSKeyValueObservingOptionNew context:&kvoContext];
    [_job addObserver:self forKeyPath:@"callbyreference" options:NSKeyValueObservingOptionNew context:&kvoContext];
}

- (void)removeObserver
{
    [_job removeObserver:self forKeyPath:@"callbyvalue"];
    [_job removeObserver:self forKeyPath:@"callbyreference"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self addObserver];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self removeObserver];
    [super viewDidDisappear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)openVc:(id)sender
{
    ViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)dismissVC:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)addObserver:(id)sender
{
    NSInteger r = arc4random_uniform(99);
    if (r % 2 == 0) {
        Observer1 *obj = [Observer1 new];
        
        [self.notifier addObserver:obj];
    }
    else
    {
        Observer2 *obj = [Observer2 new];
        
        [self.notifier addObserver:obj];
    }
    [self.notifier notify];
}

- (IBAction)removeObserver:(id)sender
{
    [self.notifier removeLast];
    [self.notifier notify];
}

- (IBAction)KVOGO:(id)sender
{
    self.job.callbyreference = @1;
    self.job.callbyreference = @2;
    self.job.callbyreference = @3;
    
    self.job.callbyvalue = 1;
    self.job.callbyvalue = 2;
    self.job.callbyvalue = 3;
}

- (IBAction)jobGo:(id)sender
{
    /*
     所有繼承NSObject的都實作這些函式。
     */
    Job *job = [Job new];
    
    [job performSelectorOnMainThread:@selector(job1) withObject:nil waitUntilDone:YES];
    [job performSelector:@selector(job2) withObject:nil afterDelay:3];
    [job performSelectorInBackground:@selector(job3:) withObject:@"Check it out!"];
    
    NSThread *thread = [[NSThread alloc] initWithTarget:job selector:@selector(job3:) object:@"我是NSThread"];
    [thread start];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(threadExitNotice) name:NSThreadWillExitNotification object:nil];
}

- (void)threadExitNotice
{
    NSLog(@"Thread finish!!");
}

- (IBAction)gcdJobGO:(id)sender
{
    /*
     dispatch_get_main_queue() -> MainQueue (UI Thread)
     */
    
    Job *job = [Job new];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [job job1];
    });
    
    /**
      使用預設的background thread來執行
     */
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [job job2];
    });
    
    /**
     DISPATCH_QUEUE_SERIAL
     DISPATCH_QUEUE_CONCURRENT
     */
    dispatch_queue_t queue = dispatch_queue_create("com.chinalife.default_queue", DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        [job job3:@"async"];
        NSLog(@"在GCD block裡驅動KVO");
        //self.job.callbyreference = @6;
    });
    
    dispatch_async(queue, ^{
        [job job3:@"sync"];
    });
    
    //自已定義Block
    dispatch_block_t job1 = ^(void) {
        [job job3:@"自已定義Block"];
        NSLog(@"在GCD block裡驅動KVO");
        //self.job.callbyreference = @4;
    };
    
    dispatch_async(queue, job2);
    dispatch_async(queue, job1);
}

- (IBAction)operationQueueGO:(id)sender
{
    NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
    NSOperationQueue *currentQueue = [NSOperationQueue currentQueue];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    queue.maxConcurrentOperationCount = 1;
    queue.suspended = NO;
    Job *job = [Job new];
    [queue addOperationWithBlock:^{
        [job job1];
        NSLog(@"在NSOperation block裡驅動KVO");
        //self.job.callbyreference = @5;
    }];
    
    [queue addOperationWithBlock:blockJob];
    
    NSOperation *operation = [[NSOperation alloc] init];
    operation.completionBlock = ^{
        [job job2];
        NSLog(@"在GCD block裡驅動KVO");
        //self.job.callbyreference = @6;
    };
    [queue addOperation:operation];
    
    CustomOperation *operation2 = [[CustomOperation alloc] initWithParam:@"自定Operation"];
    [mainQueue addOperation:operation2];
    
    CustomOperation *operation3 = [[CustomOperation alloc] initWithParam:@"自定Operation"];
    [operation3 main];
}

- (IBAction)job4:(id)sender
{
    Job *job = [Job new];
    [job job4:@"123" completion:^(NSArray * _Nullable items, NSError * _Nullable error) {
        NSLog(@"Job4 completion");
        [job job1];
    }];
}

- (IBAction)job5:(id)sender
{
    Job *job = [Job new];
    job.delegate = self;
    job.dataSource = self;
    [job job4:@"param" completion:^(NSArray *items, NSError *error) {
        NSLog(@"block finish");
        //insideJob();
    }];
}

- (IBAction)job6:(id)sender
{
    [Job job6:@999 completion:^(NSString * _Nullable error) {
        NSLog(@"Job6 : %@", error);
    }];
}

- (IBAction)job7:(id)sender
{
    Job *job = [Job new];
    [job job7:@"param" completion:^NSString * _Nonnull(NSString * _Nullable error) {
        return [NSString stringWithFormat:@"把參數回傳惹: %@", error];
    }];
}

//自已定義Block
dispatch_block_t job2 = ^(void) {
    Job *job = [Job new];
    [job job3:@"定義global的BLock"];
};

void (^blockJob)(void) = ^{
    Job *job = [Job new];
    [job job3:@"自訂Block"];
};

//MARK: - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == kvoContext)
    {
        if ([keyPath isEqualToString:@"callbyvalue"])
        {
            NSLog(@"============== callbyvalue =================");
            NSLog(@"keyPath: %@", keyPath);
            NSLog(@"object: %@", object);
            NSLog(@"change: %@", change);
        }
        else if ([keyPath isEqualToString:@"callbyreference"])
        {
            NSLog(@"============== callbyreference =================");
            NSLog(@"keyPath: %@", keyPath);
            NSLog(@"object: %@", object);
            NSLog(@"change: %@", change);
        }
    }
    else
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

//MARK: - JobDelegate, JobDataSource
- (void)job4CompleteSuccess:(NSString *)success failure:(NSError *)error
{
    NSLog(@"Job4 Delegate finished");
}

- (NSString*)whoExecuteMe
{
    return @"老張";
}

@end
